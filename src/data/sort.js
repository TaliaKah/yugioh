export default {
  Titre: "Trier par:",
  Info: [
    { type: "AZName", desc: "Noms de A à Z" },
    { type: "ZAName", desc: "Noms de Z à A" },
    { type: "AZType", desc: "Types de A à Z" },
    { type: "ZAType", desc: "Types de Z à A" },
    { type: "AZRace", desc: "Races de A à Z" },
    { type: "ZARace", desc: "Races de Z à A" },
  ],
};
