# YuGiOh

Glossaire des cartes Yu-Gi-Oh! développé à l'aide de Vue.js : https://taliakah.gitlab.io/yugioh  😁

## Project setup

```
npm install
```

### Compiles and hot-reloads for development

```
npm run serve
```

### Compiles and minifies for production

```
npm run build
```

### Lints and fixes files

```
npm run lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).
